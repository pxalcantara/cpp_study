/*
    Code to simulate the race between tortoise and hare, the rules are in exercise
    8.12 of the book "C++ how to program" 8th edition.
*/

#include <iostream>
// include rand and srand
#include <stdlib.h> 
#include <time.h>


class GameTortoiseHare {

    private:
        int n_square_;
        int t_position_;
        int h_position_;

    public:

        // int t_position;
        // int h_position;

        GameTortoiseHare (int board_size);

        ~GameTortoiseHare();

        void printPositions();

        bool checkWinner();

        void moveTortoise(int dice);

        void moveHare(int dice);

        void startRace();

        int getTortoisePosition();

        int getHarePosition();

};

    GameTortoiseHare::GameTortoiseHare(int board_size){

        n_square_ = board_size;
    }

    GameTortoiseHare::~GameTortoiseHare() {}

    void GameTortoiseHare::startRace(){

        std::cout << "BANG!!!!" << std::endl;
        std::cout << "AND THEY'RE OFF !!!!" << std::endl;
        t_position_ = 1;
        h_position_ = 1;
    }

    bool GameTortoiseHare::checkWinner(){

        bool tortoise_win = false;
        bool hare_win = false;
        
        if(t_position_ >= n_square_){
            
            tortoise_win = true;
        }
        if(h_position_ >= n_square_){

            hare_win = true;
        }

        if( tortoise_win && hare_win ){

            std::cout << " It's tie" << std::endl;
            return true;

        } else if(!tortoise_win && hare_win) {

            std::cout << "Hare wins" << std::endl;
            return true;

        } else if(tortoise_win && !hare_win){

            std::cout << "TORTOISE WIN" << std::endl;
            return true;
        } else {

            std::cout << "The race continues" << std::endl;
        }

        return false;
    }

    void GameTortoiseHare::printPositions(){

        std::cout << " Tortoise position: " << t_position_ << std::endl;
        std::cout << " Hare position: " << h_position_ << std::endl;

        // char board[n_square] = {};
        // // board initialization
        // for(int i=0; i < n_square; i++){

        //     board[i] = '|___|';
        //     std::cout << board[i];
        // }
    }

    void GameTortoiseHare::moveTortoise( int dice){
        
        int* t_position_ptr = &t_position_;

        if(1 <= dice && dice <= 5){
            std::cout << " Tortoise - Fast plod!, 3 squares to right" << std::endl;
            *t_position_ptr += 3;

        }else if (6 <= dice && dice <= 7){
            std::cout << " Tortoise - Slip, 6 squares to left" << std::endl;
            *t_position_ptr -= 6;

        }else if( 8 <= dice && dice <= 10){
            std::cout << " Tortoise - Slow plod, 1 square to right" << std::endl;
            *t_position_ptr += 1;

        }

        if (*t_position_ptr < 0){

            *t_position_ptr = 1;
            std::cout << " Tortoise - Reestart at the first square!!" << std::endl;        
        }

    }

    void GameTortoiseHare::moveHare(int dice){

        int* h_position_ptr = &h_position_;

        if(1 <= dice && dice <= 2){

            std::cout << " Hare - SLEEP, no move!!" << std::endl;
        }else if(3 <= dice && dice <= 5){

            std::cout << " Hare - BIG HOP, 9 squares to right!!" << std::endl;
            *h_position_ptr += 9;

        }else if(dice == 5){

            std::cout << " Hare - BIG SLEEP, 12 squares to left!!" << std::endl;
            *h_position_ptr -= 12;

        }else if(6 <= dice && dice <= 8){

            std::cout << " Hare - SMALL HOP, 1 square to right!!" << std::endl;
            *h_position_ptr += 1;
            
        }else if(9 <= dice && dice <= 10){

            std::cout << " Hare - SMALL Slip, 2 square to left!!" << std::endl;
            *h_position_ptr -= 2;

        }

        if (*h_position_ptr < 0){

            *h_position_ptr = 1;
            std::cout << " Hare - Reestart at the first square!!" << std::endl;        
        }
    }

    int GameTortoiseHare::getTortoisePosition(){

        return t_position_;
    }

    int GameTortoiseHare::getHarePosition(){

        return h_position_;
    }

    
int main(int argc, char* argv[]){

    int board_size = 10;
    GameTortoiseHare game(board_size);

    game.startRace();

    int t_position = game.getTortoisePosition();
    int h_position = game.getHarePosition();
    std::cout << "Tortoise position: " << t_position << std::endl;
    std::cout << "Hare position: " << h_position << std::endl; 

    

    // intialize random seed
    srand(time(NULL));

    while(true){

        // generate random numbers between 1 and 10
        int i_random = (rand() % 10) + 1;
        std::cout << "============================ "  << std::endl;
        std::cout << "i_random: " << i_random << std::endl; 
        game.moveTortoise( i_random);
        game.moveHare( i_random);
        t_position = game.getTortoisePosition();
        h_position = game.getHarePosition();
        std::cout << "Tortoise position moving " << t_position << std::endl;
        std::cout << "Hare position moving " << h_position << std::endl;
        std::cout << "============================ "  << std::endl;

        if (game.checkWinner()){
            break;
        }
    }

        

    return 0;
}