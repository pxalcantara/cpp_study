/* Exercise 8.4 of the book "C++ how to program" 8th edition, this exercise intends 
to do some manipulation using pointers 
*/
#include <iostream>


int main(int argc, char* argv[])
{
    double number1, number2;

    number1 = 7.3;

    double* fPtr;

    fPtr = &number1;

    std::cout << " fPtr: " << *fPtr << std::endl;

    number2 = *fPtr;

    std::cout << " number2: " << number2 << std::endl;
    std::cout << " number1: " << &number1 << std::endl;
    std::cout << " fPtr adress: " << fPtr << std::endl;


    return 0;
}