/* Exercise 8.3 of the book "C++ how to program" 8th edition, this exercise intend 
to show the array elements using multiple acces formats 
*/
#include <iostream>

// Show the array elements using subscription notation
void functionC (double numbers[], int array_size )
{
    for (int i=0; i < array_size ; i++)
    {
       std::cout << "Array Element: " << numbers[i] << std::endl;      

    }
    
}

// Show the element using pointers/offset notation
void functionE (double* nPtr, int array_size)
{
    for (int i=0; i < array_size; i++)
    {
        std::cout << "Array element using ptr: " << *(nPtr + i) << std::endl;
    }   
}

// Shows the elements using array as pointers offset notation
void functionF (double numbers[], int array_size)
{
    for (int i=0; i < array_size; i++)
    {
        std::cout << "Array elements using array as pointer: " << *(numbers + i) << std::endl;
        
    }

}


int main(int argc, char* argv[] )
{   
    int array_size = 10;
    double numbers[array_size] = {0.0, 1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.7, 8.8, 9.9};

    double* nPtr ; 

    nPtr = numbers; 

    // functionC(numbers, array_size);
    // functionE(nPtr, array_size);
    functionF(numbers, array_size);

    return 0;
}